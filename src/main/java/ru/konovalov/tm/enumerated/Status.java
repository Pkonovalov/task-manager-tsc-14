package ru.konovalov.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETE("Complete");

    Status(String displayName) {
        this.displayName = displayName;
    }

    private String displayName;
    public static void main(String[] args) {
        System.out.println(COMPLETE);
    }

    public String getDisplayName() {
        return displayName;
    }

}




